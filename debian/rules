#!/usr/bin/make -f

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

export DEB_LDFLAGS_MAINT_APPEND := -Wl,--as-needed -Wl,-z,defs
export DEB_CFLAGS_MAINT_APPEND := -Wall
export DEB_CXXFLAGS_MAINT_APPEND := -Wall
export DEB_BUILD_MAINT_OPTIONS := hardening=+all
# Default python version.
PYTHONDEFVERSION := $(shell pyversions -d | sed -e 's/python\([0-9.]\+\)/\1/')

include /usr/share/dpkg/pkg-info.mk

# Upstream version can be overriden so as to fetch newer revisions
# without having to modify the top-most changelog entry
UVERSION ?= $(subst +dfsg,,$(DEB_VERSION_UPSTREAM))

override_dh_makeshlibs:
	dh_makeshlibs --noscripts

override_dh_auto_install:
	dh_auto_install --verbose
	rm -vf debian/tmp/usr/share/hugin/xrc/data/help_en_EN/LICENCE.manual

override_dh_auto_clean:
	dh_auto_clean --verbose
	rm -vf src/celeste/Config.h

override_dh_auto_configure:
	dh_auto_configure --verbose --\
		-DBUILD_HSI:BOOL=ON \
		-DPythonLibs_FIND_VERSION=$(PYTHONDEFVERSION)

override_dh_python2:
	dh_python2 -i /usr/share/hugin/data/plugins
	dh_python2 -a --no-guessing-versions

%:
	dh $@ --with python2

override_dh_missing:
	dh_missing --list-missing
